const MailService = require('@sendgrid/mail');
const logger = require('heroku-logger');

MailService.setApiKey(process.env.SENDGRID_SENDMAIL_API_KEY);
const sandbox = process.env.GMT_NODE_ENV === 'dev';

const action = {
  sendMail: async (name, subject, phone, email, message) => {
    logger.info(`send mail. sandbox enabled: ${sandbox}`);

    try {
      await MailService.send({
        to: `${process.env.GMT_SERVICE_EMAIL_ADDRESS}`,
        from: email || process.env.GMT_SERVICE_EMAIL_ADDRESS,
        subject,
        html: `<p>${message}<p><a href="${email}">${name}</a><br /><a href="tel:${phone}">${phone}</a>`,
        mailSettings: {
          sandboxMode: {
            enable: sandbox
          }
        }
      });

      logger.info('Email Notifier. SUCCESS');

      return true;
    } catch (e) {
      logger.info('Email Notifier', { error: e });
    }

    return false;
  }
};

module.exports = {
  action
};
