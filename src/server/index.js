const path = require('path');
const express = require('express');
const Boom = require('boom');
const { get, isNil } = require('lodash');
const logger = require('heroku-logger');
const { action } = require('./contact/email-notifier');

const DIST_DIR = path.join(__dirname, '../../build');
const HTML_FILE = path.join(DIST_DIR, 'index.html');
const PORT = process.env.PORT || 8080;
const app = express();
const api = express();

app.use(express.static(DIST_DIR));
app.use(express.json());

api.post('/contact/email', (req, res) => {
  const { body } = req;

  const name = get(body, 'name');
  const subject = get(body, 'subject');
  const phone = get(body, 'phone');
  const email = get(body, 'email');
  const message = get(body, 'message');

  if (isNil(name) || isNil(phone) || isNil(message)) {
    logger.error('Email Contact - Attempting to contact with no details');
    res.send(Boom.badRequest('Attempting to send contact email with missing information'));
  }

  action.sendMail(name, subject, phone, email, message);
  res.send('SUCCESS');
});

app.get('*', (req, res) => {
  res.sendFile(HTML_FILE);
});

app.use('/api', api);
app.listen(PORT, () => logger.info(`Listening on port ${PORT}!`));
