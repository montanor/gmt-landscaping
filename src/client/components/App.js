import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import Header from './base/Header/index';
import Footer from './base/Footer/index';
import { Routes, RouteBuilder } from './Routes';

import 'bootstrap/dist/css/bootstrap.min.css';
import '../styles/base.less';
import '../styles/responsive.less';

const App = () => (
  <div>
    <Router>
      <div>
        <Header />
        <div className="header-spacer" />

        {Routes.map(route => (
          <RouteBuilder key={route.id} {...route} />
        ))}
      </div>
    </Router>
    <Footer />
  </div>
);

export default App;
