import React from 'react';
import { Carousel } from 'react-bootstrap';
import { Helmet } from 'react-helmet';
import Logo from '../../resources/images/logo.png';
import ImageA01 from '../../resources/images/lawn-01.png';
import ImageA02 from '../../resources/images/lawn-02.png';
import ImageA03 from '../../resources/images/lawn-03.png';
import ImageA04 from '../../resources/images/lawn-04.png';
import ImageA05 from '../../resources/images/lawn-05.png';

const Home = () => (
  <div>
    <Helmet>
      <title>Home</title>
      <meta name="description" content="Home of GMT Landscaping" />
      <meta name="keywords" content="landscaping services home" />
    </Helmet>
    <main className="main">
      <div id="home">
        <div className="section-header">
          <h3>Home</h3>
        </div>
        <div className="content">
          <div id="presentation" data-ride="carousel">
            <div className="static">
              <img src={Logo} alt="company logo" />
            </div>
            <Carousel>
              <Carousel.Item>
                <img className="d-block w-100" src={ImageA01} alt="First slide" />
              </Carousel.Item>
              <Carousel.Item>
                <img className="d-block w-100" src={ImageA02} alt="Third slide" />
                <Carousel.Caption />
              </Carousel.Item>
              <Carousel.Item>
                <img className="d-block w-100" src={ImageA03} alt="Third slide" />
                <Carousel.Caption />
              </Carousel.Item>
              <Carousel.Item>
                <img className="d-block w-100" src={ImageA04} alt="Third slide" />
                <Carousel.Caption />
              </Carousel.Item>
              <Carousel.Item>
                <img className="d-block w-100" src={ImageA05} alt="Third slide" />
                <Carousel.Caption />
              </Carousel.Item>
            </Carousel>
          </div>
          <div className="text">
            <p>
              With over seven years of experience, always family owned and operated, we at GMT
              Landscaping and Cleaning LLC are committed to providing you with an honest, efficient,
              and great quality service.
            </p>
            <p>
              Whether it is a new project, a one-time clean up, or a monthly upkeep, we do it all!
              Give us a call at
              {
                <a className="inline-anchor" href="tel:425-518-6952">
                  &nbsp;(425) 518-6952&nbsp;
                </a>
              }
              or use our
              {
                <a className="anchor inline-anchor contact-anchor" href="/contactUs">
                  &nbsp;CONTACT&nbsp;
                </a>
              }
              form to send us a message, we are happy to help.
            </p>
          </div>
        </div>
      </div>
    </main>
  </div>
);

export default Home;
