import React from 'react';
import { Helmet } from 'react-helmet';
import employee from '../../resources/images/profile-01.png';

const AboutUs = () => (
  <div>
    <Helmet>
      <title>About Us</title>
      <meta name="description" content="About GMT Landscaping" />
      <meta name="keywords" content="landscaping services about" />
    </Helmet>
    <main className="main">
      <div id="about-us">
        <div className="section-header">
          <h3>About Us</h3>
        </div>
        <div className="content">
          <div className="profile">
            <img alt="employee" className="personnel-image" src={employee} />
          </div>
          <div className="text">
            <p>
              Located in the city of North Bend, WA. GMT Landscaping and Cleaning LLC has been 
              providing top quality service since early 2010. Always family owned and operated,
              our goal is to provide our customers with a service that is both affordable and
              prompt. All without compromising quality, customer experience, or satisfaction.
            </p>
            <p>
              Specializing in residential work, we pride ourselves in providing a personable service
              that is guaranteed to meet your expectations.
            </p>
            <p>
              We invite you to give us a call, have us pay you a visit, you will be plesantly
              surprised by our personalized service and affordable prices. Whether you are looking
              for a one-time clean up or a company you can trust to maintain your gardens looking
              great month after month, you can count on us. We will make sure you get the
              professional service you deserve.
            </p>
          </div>
        </div>
      </div>
    </main>
  </div>
);

export default AboutUs;
