import React from 'react';
import { Helmet } from 'react-helmet';
import { Alert } from 'react-bootstrap';

const Oops = () => (
  <div>
    <Helmet>
      <title>Oops</title>
    </Helmet>
    <main className="main">
      <div id="oops">
        <div className="section-header">
          <h3>Oops...</h3>
        </div>
        <div className="content">
          <Alert key="oops-alert" variant="danger">
            <p>
              Something went wrong, please try again. Otherwise, please reach out to us by phone at
              <a className="inline-anchor" href="tel:425-518-6952">
                &nbsp;(425) 518-6952&nbsp;
              </a>
            </p>
            <p>Thank you!</p>
          </Alert>
        </div>
      </div>
    </main>
  </div>
);

export default Oops;
