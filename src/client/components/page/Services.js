import React from 'react';
import { Helmet } from 'react-helmet';

const Services = () => (
  <div>
    <Helmet>
      <title>Services</title>
      <meta name="description" content="GMT Landscaping services offered" />
      <meta name="keywords" content="landscaping services" />
    </Helmet>
    <main className="main">
      <div id="services">
        <div className="section-header">
          <h3>Services</h3>
        </div>
        <div className="content">
          <div className="service-list">
            <ul>
              <li>Lawn Care</li>
              <li>Tree and Plant Care</li>
              <li>Clean Ups</li>
              <li>Weed Control</li>
              <li>Maintainance</li>
              <li>Fertilizing</li>
              <li>Mulching</li>
              <li>Thatching</li>
              <li>Aerating</li>
              <li>Planting</li>
              <li>And Much More!</li>
            </ul>
          </div>
          <div className="text">
            <p>
              Unsure whether your landscaping needs are covered. Give us a call at
              {
                <a className="inline-anchor" href="tel:425-518-6952">
                  &nbsp;(425) 518-6952&nbsp;
                </a>
              }
              for more information, or use our
              {
                <a className="anchor inline-anchor contact-anchor" href="/contactUs">
                  &nbsp;CONTACT&nbsp;
                </a>
              }
              form to send us a message, we are happy to help
            </p>
          </div>
        </div>
      </div>
    </main>
  </div>
);

export default Services;
