import React from 'react';
import { Helmet } from 'react-helmet';
import { Alert } from 'react-bootstrap';

const ThankYou = () => (
  <div>
    <Helmet>
      <title>Thank You</title>
    </Helmet>
    <main className="main">
      <div id="thank-you">
        <div className="section-header">
          <h3>Thank You!</h3>
        </div>
        <div className="content">
          <Alert key="thank-you-alert" variant="success">
            <p>
              We have received your message and would like to thank you for writing to us. If you
              wish to speak with us in person, you can reach us at
              <a className="inline-anchor" href="tel:425-518-6952">
                &nbsp;(425) 518-6952&nbsp;
              </a>
            </p>
            <p>Otherwise, we will get back you by email as soon as possible.</p>
            <p>Talk to you soon, GMT Landscaping LLC</p>
          </Alert>
        </div>
      </div>
    </main>
  </div>
);

export default ThankYou;
