import React from 'react';
import axios from 'axios';
import { Helmet } from 'react-helmet';
import { get, isNil } from 'lodash';
import { Redirect } from 'react-router-dom';
import validator from 'validator';

class ContactUs extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      contact: {
        name: null,
        subject: null,
        phone: null,
        email: null,
        message: null
      },
      error: {
        name: null,
        subject: null,
        phone: null,
        email: null,
        message: null
      },
      redirect: null
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  
  hasErrors = () => {
    const { error } = this.state;

    if (error.name || error.subject || error.phone || error.email || error.message) {
      return '';
    }
    return 'visually-hidden';
  };

  isValidField = (field) => {
    const { error } = this.state;

    if (error[field]) {
      return 'alert-box';
    }
    return '';
  };

  isValidPhoneNumber = (data) => {
    const phoneNumberPattern = /^\(?(\d{3})\)?[- ]?(\d{3})[- ]?(\d{4})$/;
    return phoneNumberPattern.test(data);
  };

  handleChange(event) {
    const { id, value } = event.target;
    const { contact } = this.state;
    const info = contact;

    info[id] = value;
    this.setState({
      contact: info
    });
  }

  handleSubmit(event) {
    event.preventDefault();
    const { contact } = this.state;

    const name = get(contact, 'name');
    const phone = get(contact, 'phone');
    const email = get(contact, 'email');
    const message = get(contact, 'message');

    const { error } = this.state;
    const e = error;

    e.name = !name ? 'Please provide a name' : null;
    e.phone = !phone || !this.isValidPhoneNumber(phone) ? 'Please provide a valid phone number' : null;
    e.email = email && !validator.isEmail(email) ? 'Please provide a valid email address' : null;
    e.message = !message ? 'Please provide a message' : null;

    if (!e.name && !e.phone && !e.email && !e.message) {
      axios
        .post('/api/contact/email', contact)
        .then(res => this.setState({ redirect: get(res, 'data') === 'SUCCESS' }));
    } else {
      this.setState({
        error: e
      });
    }
  }

  render() {
    const { redirect, error } = this.state;

    if (!isNil(redirect)) {
      if (redirect) {
        return <Redirect to="/thankYou" />;
      }
      return <Redirect to="/oops" />;
    }

    return (
      <div>
        <Helmet>
          <title>Contact Us</title>
          <meta name="description" content="Contact GMT Landscaping" />
          <meta name="keywords" content="landscaping services contact phone email" />
        </Helmet>
        <main className="main">
          <div id="contact-us">
            <div className="section-header">
              <h3>Contact Us</h3>
            </div>
            <div className="content">
              <form id="contact-form" onSubmit={this.handleSubmit} noValidate>
                <div
                  id="contact-alert"
                  className={`alert alert-danger ${this.hasErrors()}`}
                  role="alert"
                >
                  <ul>
                    {Object.values(error).map((v) => {
                      if (v) {
                        return <li key={v}>{v}</li>;
                      }
                      return undefined;
                    })}
                  </ul>
                </div>
                <div className="form-group">
                  <label htmlFor="name">Name</label>
                  <i className="fa fa-asterisk" />
                  <input
                    id="name"
                    className={`form-control field ${this.isValidField('name')}`}
                    type="name"
                    onChange={this.handleChange}
                    required
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="subject">Subject</label>
                  <input
                    id="subject"
                    className="form-control field"
                    type="subject"
                    onChange={this.handleChange}
                    required
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="phone">Phone Number</label>
                  <i className="fa fa-asterisk" />
                  <input
                    id="phone"
                    className={`form-control field ${this.isValidField('phone')}`}
                    type="phone-number"
                    onChange={this.handleChange}
                    required
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="email">Email</label>
                  <input
                    id="email"
                    className={`form-control field ${this.isValidField('email')}`}
                    type="email"
                    onChange={this.handleChange}
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="message">Message</label>
                  <i className="fa fa-asterisk" />
                  <textarea
                    id="message"
                    className={`form-control field ${this.isValidField('message')}`}
                    rows="6"
                    onChange={this.handleChange}
                    required
                  />
                </div>
                <button type="submit" id="contact-submit" className="btn btn-primary">
                  Submit
                </button>
              </form>
            </div>
          </div>
        </main>
      </div>
    );
  }
}

export default ContactUs;
