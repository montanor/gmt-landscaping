import React from 'react';
import { Route } from 'react-router-dom';
import Home from './page/Home';
import AboutUs from './page/AboutUs';
import Services from './page/Services';
import ContactUs from './page/ContactUs';
import ThankYou from './page/ThankYou';
import Oops from './page/Oops';

export const Routes = [
  {
    id: 'home',
    path: '/',
    component: Home
  },
  {
    id: 'aboutUs',
    path: '/aboutUs',
    component: AboutUs
  },
  {
    id: 'services',
    path: '/services',
    component: Services
  },
  {
    id: 'contactUs',
    path: '/contactUs',
    component: ContactUs
  },
  {
    id: 'thankYou',
    path: '/thankYou',
    component: ThankYou
  },
  {
    id: 'oops',
    path: '/oops',
    component: Oops
  }
];

export const RouteBuilder = route => (
  <Route exact path={route.path} render={props => <route.component {...props} />} />
);
