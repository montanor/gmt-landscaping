import React from 'react';
import { NavLink } from 'react-router-dom';
import Nav from 'react-bootstrap/Nav';

class MobileNavMenu extends React.Component {
  constructor(props) {
    super(props);
    this.state = { mobileMenuOpened: false };
  }

  toggleNav = () => {
    const { mobileMenuOpened } = this.state;

    if (mobileMenuOpened) {
      this.setState({ mobileMenuOpened: false });
    } else {
      this.setState({ mobileMenuOpened: true });
    }
  };

  navVisual = () => {
    const { mobileMenuOpened } = this.state;
    return mobileMenuOpened ? '' : 'visually-hidden';
  };

  render() {
    return (
      <div>
        <div className="mobile-menu">
          <button onClick={this.toggleNav} type="submit">
            <i className="fa fa-bars" />
          </button>
        </div>
        <button
          onClick={this.toggleNav}
          className={`mobile-backdrop ${this.navVisual()}`}
          type="submit"
        />
        <Nav className={['mobile-nav animate-drawer', this.navVisual()]} variant="pills">
          <div className="nav-close" />
          <Nav.Item>
            <NavLink
              onClick={this.toggleNav}
              activeClassName="active"
              className="nav-link"
              to="/"
              exact
            >
              HOME
            </NavLink>
          </Nav.Item>
          <Nav.Item>
            <NavLink
              onClick={this.toggleNav}
              activeClassName="active"
              className="nav-link"
              to="/aboutUs"
            >
              ABOUT US
            </NavLink>
          </Nav.Item>
          <Nav.Item>
            <NavLink
              onClick={this.toggleNav}
              activeClassName="active"
              className="nav-link"
              to="/services"
            >
              SERVICES
            </NavLink>
          </Nav.Item>
          <Nav.Item>
            <NavLink
              onClick={this.toggleNav}
              activeClassName="active"
              className="nav-link"
              to="/contactUs"
            >
              CONTACT US
            </NavLink>
          </Nav.Item>
        </Nav>
      </div>
    );
  }
}

export default MobileNavMenu;
