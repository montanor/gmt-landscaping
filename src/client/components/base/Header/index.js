import React from 'react';
import { NavLink } from 'react-router-dom';
import MobileNavMenu from './MobileNavMenu';
import NavMenu from './NavMenu';

class Header extends React.Component {
  constructor(props) {
    super(props);
    this.state = { init: true, shrinking: false };
  }

  componentDidMount = () => {
    window.addEventListener('scroll', this.shrink.bind(this));
  };

  componentWillUnmount = () => {
    window.removeEventListener('scroll', this.shrink.bind(this));
  };

  shrink = () => {
    if (document.body.scrollTop > 25 || document.documentElement.scrollTop > 25) {
      this.setState({ init: false, shrinking: true });
    } else {
      this.setState({ shrinking: false });
    }
  };

  shrinkingHeader = () => {
    const { shrinking, init } = this.state;
    if (init) return '';

    return shrinking ? 'shrinking-header' : 'expanding-header';
  };

  render() {
    return (
      <header className={this.shrinkingHeader()}>
        <div className="boxed">
          <div className="brand">
            <div className="logo">
              <div className="leaf-1" />
              <div className="leaf-2" />
            </div>
            <h2 className="name">
              <NavLink to="/">GMT Landscaping</NavLink>
            </h2>
          </div>
          <NavMenu />
          <MobileNavMenu />
        </div>
      </header>
    );
  }
}

export default Header;
