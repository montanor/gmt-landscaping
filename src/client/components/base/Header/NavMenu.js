import React from 'react';
import { NavLink } from 'react-router-dom';
import Nav from 'react-bootstrap/Nav';

const NavMenu = () => (
  <Nav className="base-nav" variant="pills">
    <Nav.Item>
      <NavLink activeClassName="active" className="nav-link" to="/" exact>
        HOME
      </NavLink>
    </Nav.Item>
    <Nav.Item>
      <NavLink activeClassName="active" className="nav-link" to="/aboutUs">
        ABOUT US
      </NavLink>
    </Nav.Item>
    <Nav.Item>
      <NavLink activeClassName="active" className="nav-link" to="/services">
        SERVICES
      </NavLink>
    </Nav.Item>
    <Nav.Item>
      <NavLink activeClassName="active" className="nav-link" to="/contactUs">
        CONTACT US
      </NavLink>
    </Nav.Item>
  </Nav>
);

export default NavMenu;
