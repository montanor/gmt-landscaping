import React from 'react';

const Footer = () => (
  <footer>
    <div className="boxed">
      <div className="contact">
        <ul>
          <li>43010 SE North Bend Way 11</li>
          <li>North Bend WA. 98045</li>
          <li>
            <a href="tel:425-518-6952">425.518.6952</a>
          </li>
        </ul>
      </div>
      <div className="social">
        <a className="btn btn-default btn-sm" href="https://www.facebook.com/gmtlandscaping/">
          <i className="fab fa-facebook-square" />
        </a>
        <a className="btn btn-default btn-sm" href="https://twitter.com/GmtLandscaping/">
          <i className="fab fa-instagram" />
        </a>
        <a className="btn btn-default btn-sm" href="https://twitter.com/GmtLandscaping/">
          <i className="fab fa-youtube-square" />
        </a>
        <a className="btn btn-default btn-sm" href="https://twitter.com/GmtLandscaping/">
          <i className="fab fa-twitter-square" />
        </a>
      </div>
      <div className="author">
        <ul>
          <li>GMT Landscaping and Cleaning, LLC</li>
          <li>2017 All rights reserved</li>
          <li>
            <a href="http://www.linkedin.com/in/montanorx">montanorx</a>
          </li>
        </ul>
      </div>
    </div>
  </footer>
);
export default Footer;
