# GMT Landscaping LLC

Responsive Web. Home of GMT. NodeJS, Webpack, ExpressJS, React

- [Development mode](#development-mode)
- [Production mode](#production-mode)
- [Quick Start](#quick-start)
- [Documentation](#documentation)
  - [Folder Structure](#folder-structure)
  - [Installation guide](#installation-guide)

## Development mode

Development mode on 2 different server. Nodemon, for running the expressJS and help with live reloading. Webpack-Dev-Server, for running the front end code, also helps for hot/live reloading.

## Quick Start

```bash
# Clone the repository
git clone git@gitlab.com:montanor/gmt-landscaping.git

# Go inside the directory
cd gmt-landscaping

# Install dependencies
npm install

# Start development server
npm run dev

# Build for production
npm build

# Start production server
npm start
```

## Documentation

### Folder Structure

All source is located under the ./src folder.

Inside this folder. ./client holds all of the UI components. ./server holds all of the BFF logic

#### Installation guide

In order to prevent the BFF email server from fully processing emails. You must set the following environment variable:

export GMT_NODE_ENV=dev

Without this variable, the email server will attempt to process emails and fail. After this variable is been set, follow the 'quick start' guide
